using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[CreateAssetMenu(menuName = "Game/EventCenter")]
public class EventCenter : ScriptableObject
{

    public UnityAction<string> debugEvent;

    public UnityAction<float> lightBarValueChange;

    public UnityAction firstBad;

    public UnityAction finish;

}
