using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

public enum Status { normal, bad, restart, finish }
public class JellyfishController : MonoBehaviour
{

    private Rigidbody2D rb;
    private float lightBarValue = 1.0f;
    private Light2D light;
    private bool recovering = false;
    private Vector3 startPoint;
    private bool disable = false;
    private bool recoveringZone = false;
    private Status status;
    private bool firstBad = true;
    private int badCount = 0;

    public float maxSpeed;
    public float score;
    public bool enableZoom;
    public float bubbleSpeed;
    [Range(0, 1)]
    public float bubbleChance;

    public GameObject particleSystem;
    public EventCenter ec;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        light = transform.Find("Light").GetComponent<Light2D>();
        transform.Find("Global").GetComponent<Light2D>().intensity = 0.1f;
        startPoint = transform.position;
        setStatus(Status.normal);
    }

    private void Update()
    {
        if (disable)
        {
            return;
        }
        var tp = transform.position;
        Camera.main.transform.position = new Vector3(tp.x, tp.y, -1);
        Vector2 p = new Vector2(Input.mousePosition.x - Screen.width / 2,
                    Input.mousePosition.y - Screen.height / 2);
        var angle = -Vector2.SignedAngle(p, Vector2.up);
        if (status == Status.bad && !firstBad)
        {
            angle += Random.Range(-10.0f, 10.0f) * badCount;
        }
        transform.rotation = Quaternion.Euler(0, 0, angle);

        if (Input.GetMouseButton(0) || Input.GetKey(KeyCode.Space))
        {
            float sy = transform.localScale.y;
            transform.localScale = new Vector3(1, Mathf.Lerp(sy, 0.7f, Time.deltaTime), 1);
            if (enableZoom && !recoveringZone)
            {
                float cs = Camera.main.orthographicSize;
                Camera.main.orthographicSize = Mathf.Lerp(cs, 5, Time.deltaTime);
            }
        }
        else if (Input.GetMouseButtonUp(0) || Input.GetKeyUp(KeyCode.Space))
        {
            rb.velocity = Vector2.zero;
            float sy = transform.localScale.y;
            Vector3 v = transform.up.normalized * maxSpeed * (1 - sy);
            if (status == Status.bad && !firstBad)
            {
                v *= Random.Range(0.5f / badCount, 1.1f + badCount / 10.0f);
            }
            rb.AddForce(new Vector2(v.x, v.y));
        }
        else
        {
            transform.localScale = new Vector3(1, 1, 1);
            if (enableZoom)
            {
                float cs = Camera.main.orthographicSize;
                if (recoveringZone)
                {
                    Camera.main.orthographicSize = Mathf.Lerp(cs, 12, Time.deltaTime / 2);
                } 
                else
                {
                    Camera.main.orthographicSize = Mathf.Lerp(cs, 6, Time.deltaTime * 10);
                }
            }
        }
        light.pointLightOuterRadius = lightBarValue * 10 + 3;
        light.intensity = lightBarValue / 3 + 0.67f;
        if (lightBarValue < 0.5f)
        {
            light.pointLightOuterRadius += (Random.Range(1.0f, 10.0f) - 5) / 10;
        }
    }

    private void FixedUpdate()
    {
        if (disable)
        {
            return;
        }
        if (rb.velocity.magnitude > bubbleSpeed && Random.Range(0, 1.0f) < bubbleChance)
        {
            GameObject ins = Instantiate(particleSystem);
            ins.transform.position = transform.position;
            ins.transform.rotation = transform.rotation;
            StartCoroutine(doPs(ins));
        }
        if (!recovering && !recoveringZone)
        {
            LightBarValueChange(-Time.fixedDeltaTime / 10);
        }
    }

    private void LightBarValueChange(float change)
    {
        lightBarValue += change;
        if (lightBarValue < -0.2f)
        {
            lightBarValue = 0;
            disable = true;
            StartCoroutine(restart());
        }
        if (lightBarValue > 1)
        {
            lightBarValue = 1;
        }
        ec.lightBarValueChange.Invoke(lightBarValue);
    }

    private IEnumerator doPs(GameObject ins)
    {
        //GetComponent<AudioSource>().Play();
        ParticleSystem ps = ins.transform.Find("ParticleSystem").GetComponent<ParticleSystem>();
        ps.Play();
        yield return new WaitForSeconds(ps.main.startLifetime.constant * 2);
        Destroy(ins);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("collect"))
        {
            startPoint = collision.transform.position;
            Destroy(collision.gameObject);
            maxSpeed += score;
            StartCoroutine(addEnergy(1));
        }
        else if (collision.gameObject.CompareTag("bad"))
        {
            setStatus(Status.bad);
            Destroy(collision.gameObject);
            GetComponent<SpriteRenderer>().color = collision.GetComponent<SpriteRenderer>().color;
            light.color = collision.transform.Find("light").GetComponent<Light2D>().color;
            LightBarValueChange(Random.Range(-0.1f, 0.2f));
            if (firstBad)
            {
                ec.firstBad.Invoke();
                firstBad = false;
            }
            badCount++;
        }
        else if (collision.gameObject.CompareTag("Player"))
        {
            startPoint = collision.transform.position;
            Destroy(collision.gameObject);
            maxSpeed += score * 10;
            StartCoroutine(addEnergy(5));
        }
        else if (collision.gameObject.CompareTag("Respawn"))
        {
            recoveringZone = true;
            setStatus(Status.normal);
            startPoint = collision.transform.position;
            LightBarValueChange(1);
        }
        else if (collision.gameObject.CompareTag("Finish"))
        {
            recoveringZone = true;
            setStatus(Status.finish);
            ec.finish.Invoke();
            LightBarValueChange(1);
        }
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Respawn"))
        {
            recovering = false;
            recoveringZone = false;
        }
    }

    private IEnumerator addEnergy(int power)
    {
        recovering = true;
        for (int i = 0; i < 10 * power; i++)
        {
            yield return new WaitForSeconds(0.1f);
            LightBarValueChange(0.05f * power);
        }
        recovering = false;
    }

    private IEnumerator restart()
    {
        setStatus(Status.restart);
        for (int i = 0; i < 20; i++)
        {
            yield return new WaitForSeconds(0.05f);
            light.intensity -= 0.05f;
        }
        transform.position = startPoint;
        LightBarValueChange(1);
        disable = false;
        setStatus(Status.normal);
    }

    private void setStatus(Status s)
    {
        status = s;
        ec.debugEvent.Invoke(s.ToString());
        if (s != Status.bad)
        {
            GetComponent<SpriteRenderer>().color = Color.white;
            light.color = Color.white;
            badCount = 0;
        }
    }

}
