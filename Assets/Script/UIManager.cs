using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    private Transform panel;
    private Transform badPanel;
    private Transform debugPanel;
    private Transform tfp;
    private Image lightBar;
    private float target;

    public EventCenter ec;

    private void Awake()
    {
        Time.timeScale = 0;
    }

    private void Start()
    {
        if (Application.version.ToUpperInvariant().Contains("CN"))
        {
            panel = transform.Find("PanelCN");
            badPanel = transform.Find("BadPanelCN");
            tfp = transform.Find("tfpCN");
            transform.Find("Panel").gameObject.SetActive(false);
            transform.Find("BadPanel").gameObject.SetActive(false);
            transform.Find("tfp").gameObject.SetActive(false);
        }
        else
        {
            panel = transform.Find("Panel");
            badPanel = transform.Find("BadPanel");
            tfp = transform.Find("tfp");
            transform.Find("PanelCN").gameObject.SetActive(false);
            transform.Find("BadPanelCN").gameObject.SetActive(false);
            transform.Find("tfpCN").gameObject.SetActive(false);
        }

        badPanel.gameObject.SetActive(false);
        tfp.gameObject.SetActive(false);

        debugPanel = transform.Find("DebugPanel");
        debugPanel.gameObject.SetActive(false);


        lightBar = transform.Find("LightBar").GetComponent<Image>();
        lightBar.gameObject.SetActive(false);

        target = 1;
    }

    private void OnEnable()
    {
        ec.lightBarValueChange += LightBarChange;
        ec.firstBad += firstBad;
        ec.finish += finish;
        ec.debugEvent += debug;
    }

    private void OnDisable()
    {
        ec.lightBarValueChange -= LightBarChange;
        ec.firstBad -= firstBad;
        ec.finish -= finish;
        ec.debugEvent -= debug;
    }

    private void Update()
    {
        lightBar.fillAmount = Mathf.Lerp(lightBar.fillAmount, target, Time.deltaTime);
        if (Input.GetKeyDown(KeyCode.F3))
        {
            debugPanel.gameObject.SetActive(!debugPanel.gameObject.activeSelf);
        }
    }

    public void Pu()
    {
        panel.gameObject.SetActive(false);
        badPanel.gameObject.SetActive(false);
        lightBar.gameObject.SetActive(true);
        Time.timeScale = 1;
    }

    private void LightBarChange(float value)
    {
        target = value;
    }

    private void firstBad()
    {
        Time.timeScale = 0;
        badPanel.gameObject.SetActive(true);
    }

    private void finish()
    {
        tfp.gameObject.SetActive(true);
    }

    private void debug(string str)
    {
        if (debugPanel != null)
        {
            debugPanel.Find("Text").GetComponent<Text>().text = str;
        }
    }


}
